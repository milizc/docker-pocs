## PoCs con aspectos de seguridad Docker
Este proyecto incluye PoCs de Docker, haciendo énfasis en aspectos de seguridad.
Desarrollado en conjunto con Ing. Ileana Barrionuevo. 

### Requisitos

- Ejcutarlo en un SO GNU/Linux. Se lo probó en Ubuntu y Debian
- Tener instalado [Docker](https://docs.docker.com/engine/install/) 
- Tener instalado [Docker-compose](https://docs.docker.com/compose/install/)


## Pasos de reproducción
* 1- Clonar este repositorio. Cada carpeta representa un Poc distinta, ademas cada PoC tiene un script `config.sh`
* 3- Para ejecutar cualquier Poc, darle permisos de ejecución al script `config.sh`.
* 4- En el caso que la PoC tenga un `docker-compose.yml`. Ejecutar `docker-compose up -d`, asegurar se estar
  parado a la misma altura de este archivo. Ya que este comando busca el archivo `docker-compose.yml` para leerlo



> Sponsored by [Docker: atacar para defender](https://docs.google.com/presentation/d/1bFQliAJoFe-Uj2PLcDEc0eo_JrZRCYv1GTppJG5TXDQ/edit?usp=sharing)
> and [Laboratorio de Sistemas UTN - FRC (LabSis)]()


