#!/bin/bash
#Esta Poc demuestra que sin la configuración correcta de docker y el firewall de la máquina host, el demon de docker
# puede tener control de los puertos abiertos.
apt-get install ufw
sudo ufw default deny incoming #denegar todo por default
sudo ufw allow ssh #ideal solo permitir a algunas IP no anywhere
sudo ufw allow http
sudo ufw allow mysql

#Dcker se bypasea al ufw para hacer el bind  de puertos del conenedor con el hosts.
#Para solucionar esto agregamos o creamos el achivo /etc/docker/daemon.json:
sudo echo '{ "iptables": false }' > /etc/docker/daemon.json
sudo service docker restart