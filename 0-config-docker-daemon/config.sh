#!/bin/bash

#Creamos la clave privada y certificado de CA.
openssl genrsa -out ./ca/ca-key.pem 4096
openssl req -x509 -new -nodes -key ./ca/ca-key.pem -days 3650 -out ./ca/ca.pem -subj '/CN=docker-CA'

#Creamos el certificado del cliente
openssl genrsa -out ./cliente/client-key.pem 4096
openssl req -new -key ./cliente/client-key.pem -out ./cliente/client-cert.csr -subj '/CN=docker-client' -config ./ca/openssl.cnf
openssl x509 -req -in ./cliente/client-cert.csr -CA ./ca/ca.pem -CAkey ./ca/ca-key.pem -CAcreateserial -out ./cliente/client-cert.pem -days 3650 -extensions v3_req -extfile ./ca/openssl.cnf

#Creamos los certificados que van a ser usados por docker
#mkdir /etc/docker/ssl
chmod 700 /etc/docker/ssl

#Creamos los certificados que van a ser usados por docker
cp ./ca/ca.pem /etc/docker/ssl/
cp ./daemon/openssl.cnf /etc/docker/ssl/


openssl genrsa -out /etc/docker/ssl/daemon-key.pem 4096
openssl req -new -key /etc/docker/ssl/daemon-key.pem -out /etc/docker/ssl/daemon-cert.csr -subj '/CN=docker-daemon' -config /etc/docker/ssl/openssl.cnf
openssl x509 -req -in /etc/docker/ssl/daemon-cert.csr -CA /etc/docker/ssl/ca.pem -CAkey ./ca/ca-key.pem -CAcreateserial -out /etc/docker/ssl/daemon-cert.pem -days 365 -extensions v3_req -extfile /etc/docker/ssl/openssl.cnf

chmod 600 /etc/docker/ssl/*

cp ./daemon/daemon.json /etc/docker/


#Cambiar ExecStart=/usr/bin/dockerd -H fd:// a ExecStart=/usr/bin/dockerd en /lib/systemd/system/docker.service.
#systemctl daemon-reload
#service docker start


#https://docs.docker.com/config/daemon/